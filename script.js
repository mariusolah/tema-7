let chapter = Math.floor(Math.random() * 21) +1;
let verse = Math.floor(Math.random() *20) +1;

const fetchVerse = () => {
    $('#div').addClass('lds-ellipsis');
    fetch(`https://cors-anywhere.herokuapp.com/https://bible-api.com/Ioan+${chapter}:${verse}?translation=rccv`)
    .then(el => {
        return el.json();
    })
    .then(el => {
        $('#div').removeClass('lds-ellipsis');
        $('#div2').text(el.text);
        $('#div3').text(el.reference);
        console.log(el)
    })
    .catch(error => $('#div2').text(error));
}

fetchVerse();